ARG MavenVersion

FROM maven:${MavenVersion}

CMD ["mvn"]
